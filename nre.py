import nltk
import codecs
import wikipedia
from string import punctuation
from nltk.corpus import stopwords

def extractEntities(ne_chunked):
    data = {}
    for entity in ne_chunked:
        if isinstance(entity, nltk.tree.Tree):
            text = " ".join([word for word, tag in entity.leaves()])
            ent = entity.label()
            data[text] = ent
        else:
            continue
    return data

def extractNRE(ne_chunked):
    data = {}
    for entity in ne_chunked:
        if isinstance(entity, nltk.tree.Tree):
            if (not entity.leaves()[0][0][0].isupper()) or (entity.leaves()[0][1].startswith("DT") and not entity.leaves()[1][0][0].isupper()):
                continue
            text = " ".join([word for word, tag in entity.leaves()])
            if len(text) < 3:
                continue
            ent = entity.label()
            data[text] = ent
        else:
            continue
    return data

def wikipediaClassify(entities):
    res = {}
    for entity in entities:
        if len(wikipedia.search(entity)):
            try:
                summary = wikipedia.page(entity).summary
            except:
                res[entity] = "unknown"
                continue
            sent = nltk.sent_tokenize(summary)
            if len(sent):
                sent = sent[0]
            else:
                res[entity] = "unknown"
                continue
            words = nltk.word_tokenize(sent)
            words = [w for w in words if w not in punctuation]
            tagged = nltk.pos_tag(words)

            grammar = """
                    P1: {<VBZ><DT>*<JJ>*<NN|NNS|NNP>+}
                """
            custom_ner = nltk.RegexpParser(grammar)
            ents = extractEntities(custom_ner.parse(tagged))
            if (len(ents)):
                words = nltk.word_tokenize(ents.keys()[0])
                words = [w for w in words if w not in stopwords.words("english")]
                res[entity] = " ".join(words)
            else:
                res[entity] = "unknown"
        else:
            res[entity] = "unknown"
    return res

with codecs.open("corpus.ascii.txt", encoding="utf-8") as f:
    corpus = f.read()

stops = stopwords.words("english")
tokens = nltk.word_tokenize(corpus)
filtered_tokens = [t for t in tokens if t not in punctuation]
filtered_tokens = [t for t in filtered_tokens if t not in stops]
tagged = nltk.pos_tag(filtered_tokens)
# nltk_entities = extractEntities(nltk.ne_chunk(tagged, binary=True))
# with open('entities_nltk.txt', 'w') as of:
#     for ent in nltk_entities:
#         of.write(ent + "\n")
# with open('entities_nltk.csv', 'w') as of:
#     of.write("entity,class\n")
#     for entity,cl in wikipediaClassify(nltk_entities.keys()[::10]).items():
#         of.write(entity + "," + cl + "\n")

grammar = "NP: {<DT>?<JJ>*<NNP>+}"
custom_ner = nltk.RegexpParser(grammar)
grammar_entities = extractNRE(custom_ner.parse(tagged))
with open('entities_grammar.txt', 'w') as of:
    for ent in grammar_entities.keys():
        of.write(ent + "\n")
with open('entities_grammar.csv', 'w') as of:
    of.write("entity,class\n")
    for entity,cl in wikipediaClassify(grammar_entities.keys()[::10]).items():
        if (entity[0].isupper()):
            of.write(entity + "," + cl + "\n")

# entities = []
# entity = []
# for token in tagged:
#     if(token[1].startswith("NN") or (entity and token[1].startswith("IN"))):
#         entity.append(token)
#     else:
#         if (entity) and entity[-1][1].startswith("IN"):
#             entity.pop()
#         if(entity and " ".join(e[0] for e in entity)[0].isupper()):
#             ne = " ".join(e[0] for e in entity)
#             if ne not in entities:
#                 entities.append(ne)
#         entity = []
# with open('entities_custom.txt', 'w') as of:
#     for ent in entities:
#         of.write(ent + "\n")
# with open('entities_custom.csv', 'w') as of:
#     of.write("entity,class\n")
#     for ne,cl in wikipediaClassify(entities[::10]).items():
#         of.write(ne + "," + cl + "\n")
    
